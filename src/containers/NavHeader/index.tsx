/* Global imports */
import * as React from 'react'
import Header from '../../components/Header'

/* Local imports */

/* Types  */

type Props = { title?: string; color?: string }
/* Local utility functions */

/* Component definition */
const NavHeader = (props: Props) => {
  const { title, color } = props

  return (
    <>
      <Header color={color} title={`Hoy me siento:${title}`} />
    </>
  )
}

/* Styles */

export default NavHeader
