// Global imports
import React from 'react'

// Local imports
import VeryExpensiveComponent from './components/VeryExpensiveComponent'

import './App.css'
import NavHeader from './containers/NavHeader'
import Button from './components/Button'
import List from './components/List'

/* Types  */
export type Sentiment = {
  name: string
  color: string
}

type Action =
  | { type: 'change_text'; sentiment: Sentiment }
  | { type: 'change_color'; color: string | undefined }

type AppState = {
  sentiments: Sentiment[]
  selected?: Sentiment
  color?: string
}

/* Local utility functions */

const reducer = (state: AppState, action: Action): AppState => {
  switch (action.type) {
    case 'change_text': {
      return { ...state, selected: action.sentiment }
    }
    case 'change_color': {
      return { ...state, color: action.color }
    }
  }
}
const COLORS: { [key: string]: string } = {
  positivo: 'green',
  negativo: 'red',
  neutro: 'orange',
}
function App() {
  let sentiments: any = ['positivo', 'negativo', 'neutro']

  sentiments.map((s: string) => {
    return { name: s, color: COLORS[s] }
  })

  const [state, dispatch] = React.useReducer(reducer, {
    sentiments: sentiments,
    color: undefined,
    selected: undefined,
  })

  const handleClick = (item: Sentiment) => {
    dispatch({ type: 'change_text', sentiment: item })
  }

  const changeColor = () =>
    dispatch({ type: 'change_color', color: state.selected?.color })

  return (
    <div className="App">
      <NavHeader color={state.color} title={state.selected?.name} />
      {/* center me please */}
      <Button onClick={changeColor}>Change text color</Button>
      <List items={state.sentiments} onItemClick={handleClick} />
      <VeryExpensiveComponent />
    </div>
  )
}
export default App
