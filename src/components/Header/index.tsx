/* Global imports */
import * as React from 'react'
import styled from 'styled-components'

/* Local imports */

/* Types  */

type Props = {
  title: string
  color?: string
}
/* Local utility functions */

/* Component definition */
const Header = (props: Props) => {
  const { title, color } = props

  return <H1 color={color}>{title}</H1>
}

/* Styles */

const H1 = styled.h2<{ color?: string }>`
  ${(props) =>
    props.color &&
    `
      color: ${props.color};
    `}
`
export default Header
