/* Global imports */
import * as React from 'react'
import styled from 'styled-components'
import { Sentiment } from '../../App'

/* Local imports */

/* Types  */

type Props = {
  items: Sentiment[]
  onItemClick: any
}
/* Local utility functions */

/* Component definition */
const List = (props: Props) => {
  const { items, onItemClick } = props

  return (
    <Ul>
      {items.map((item, index) => (
        <Li key={index.toString()} color={item.color} onClick={onItemClick}>
          {item.name}
        </Li>
      ))}
    </Ul>
  )
}

/* Styles */
const Ul = styled.ul`
  margin: 1rem 0rem;
  padding: 0rem;
`

const Li = styled.li`
  align-items: center;
  display: flex;
  flex-direction: row;
  font-weight: bolder;
  justify-content: center;
  letter-spacing: 0.1rem;
  cursor: pointer;
  list-style: none;
  padding: 1rem 0rem;
  text-transform: uppercase;
`

export default List
