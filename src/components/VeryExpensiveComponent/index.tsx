/* Global imports */
import * as React from 'react'
import styled from 'styled-components'
/* Local imports */

/* Types  */

/* Local utility functions */

/* Component definition */
const VeryExpensiveComponent = () => {
  let now = performance.now()
  while (performance.now() - now < 100) {}
  return <Wrapper>Soy un arbol de componentes muy lento .</Wrapper>
}

/* Styles */

const Wrapper = styled.div`
  border: 4px dashed purple;
  margin: 1rem;
  padding: 2rem;
`

export default VeryExpensiveComponent
