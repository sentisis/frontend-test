//
// Me gustaría tener un aspecto más decente
// ¿Me cambias un poco?
//

import * as React from 'react'
import styled from 'styled-components'

/* Local imports */

/* Types  */
type Props = {
  onClick: React.MouseEventHandler
  children: React.ReactNode
}

/* Local utility functions */

/* Component definition */
const Button = ({ onClick, children }: Props) => {
  return <CustomButton onClick={onClick}>{children}</CustomButton>
}

/* Styles */
const CustomButton = styled.button`
  background-color: #3b3ba5;
  color: white;
  border-radius: 0.5rem;
  border: none;
  display: flex;
  margin: 1rem;
  max-width: 10rem;
  min-width: 3rem;
  padding: 1rem;
  cursor: pointer;
`

export default Button
