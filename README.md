# Séntisis FrontEnd Test

Se trata de una pequeña aplicación donde se muestran, en principio, cuatro componentes principales: 
- Un texto 
- Un Botón
- Una lista de elementos
- Un componente bastante costoso en consumo de recursos 

El funcionamiento correcto debería ser el siguiente: 

El usuario, al hacer clic sobre un elemento de la lista, el texto ***"Hoy me siento:"***  debería tomar el valor del texto de la lista.

De la misma forma, al hacer clic sobre el botón, el texto ***"Hoy me siento: "*** debería reflejar el mismo color que el elemento seleccionado de la lista.


## Algunas preguntas sobre React
- ¿Podrías explicar un poco qué es el props drilling? ¿Cómo lo evitarías?

- ¿Y un High Order Component?

- ¿Qué diferencia existe entre un stateless vs stateful component ?

- ¿Sabrías decir en qué consiste el término Reconciliation en React ?
  
- ¿Sabes qué es la programación funcional? 

- ¿Qué toolbelts de apoyo usas en tu día a día en tus desarrollos ?